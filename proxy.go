package hoerzu

// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.

import (
	"io"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

// client deliver the
// out-bound server traffic to the clients.
type client struct {
	// back link to the session which is connected
	// to this eavesdropper.
	session *session

	// is set to true if this is a bi-directional client.
	bidirectional bool

	// the established connection to the client.
	conn net.Conn

	// a queue of server data packets protected
	// by a condition to make braoadcasting not
	// depended to the delivery speed of the
	// clients.
	buffersC *sync.Cond
	buffers  *bufferQueue
}

// serverClosedSentinel is not a real buffer.
// Its used as signal/marker send from the server
// to the connected clients that the server will
// not send any more buffers.
var serverClosedSentinel = new(buffer)

// sessionState models the different internal states
// of a session. Used to maintain the session running
// and be able to shut it down cleanly.
type sessionState int

const (
	openState             sessionState = 0
	serverClosedReadState sessionState = 1 << iota
	serverClosedWriteState
	clientClosedState
	waitRemainingClientsState
	closedState = serverClosedReadState |
		serverClosedWriteState | clientClosedState
)

func (s sessionState) has(m sessionState) bool {
	return s&m == m
}

func (s sessionState) any(m sessionState) bool {
	return s&m != 0
}

func (s sessionState) String() string {

	if s == openState {
		return "open"
	}
	var flags []string
	for _, x := range []struct {
		m sessionState
		t string
	}{
		{serverClosedReadState, "server closed read"},
		{serverClosedWriteState, "server closed write"},
		{clientClosedState, "client closed"},
		{waitRemainingClientsState, "wait remaining clients"},
	} {
		if s.has(x.m) {
			flags = append(flags, x.t)
		}
	}
	return strings.Join(flags, "|")
}

// session is the essential connection between the
// remote server and the clients. There is only
// one bi-directional client and optional many
// eavesdroppers.
type session struct {

	// back link to the proxy which started this session.
	proxy *Proxy

	// the established connection to the remote server.
	server net.Conn
	// the established connection to the bi-directional client.
	client net.Conn

	// the list of clients.
	clients []*client

	// command channel to manage the session
	// data structure from different go routines.
	cmds chan func(*session)

	// channel of data packets received from server
	// to be broadcast to the clients.
	in chan *buffer

	// the current state of the session.
	state sessionState

	// if pressure is set it is used to signal the server
	// to stop reading until the pressure is relaxed.
	// This is managed with the condition pressureC.
	pressureC *sync.Cond
	pressure  bool

	// relax is used by the client to signal that
	// the pressure of there repspective buffers is
	// relaxed. The session updates pressure accordingly.
	relax chan (struct{})
}

// Proxy manages the life-cycle of the listening servers of
// the system and spawns new sessions if new bi-directional
// clients connect to it.
type Proxy struct {

	// configuration of the remote server to
	// be connected by this proxy.
	serverAddr string
	serverPort int

	// configuration of the listen address of the
	// bi-directional clients connecting to this proxy.
	listenAddr string
	listenPort int

	// configuration of the eavesdropper clients
	// connecting to this client.
	eavesdropAddr string
	eavesdropPort int

	// timeout before a stale client is disconnected.
	timeout time.Duration

	// the bound interface for the bi-directional clients.
	listenListener net.Listener
	// the bound interface for the eavesdropper clients.
	eavesdropListener net.Listener

	// command channel to manage the proxy data structure
	// w/o races from different go routines.
	cmds chan func(*Proxy)

	// the list of the currently active sessions.
	sessions []*session

	// set to true if the proxy is gone.
	done bool

	// clean-up callback after all session are gone.
	noMoreSessions func(*Proxy)
}

// closeReadWrite is a common interface to half open
// net work interfaces net.TCPConn and net.UnixConn.
// Alternative would be hard coding of net.TCPConn.
type closeReadWrite interface {
	CloseRead() error
	CloseWrite() error
}

func newClient(
	s *session,
	conn net.Conn,
	bidirectional bool) *client {

	return &client{
		session:       s,
		conn:          conn,
		bidirectional: bidirectional,
		buffersC:      sync.NewCond(new(sync.Mutex)),
		buffers:       newBufferQueue(),
	}
}

func newSession(p *Proxy) *session {
	return &session{
		proxy:     p,
		cmds:      make(chan func(*session), 2),
		in:        make(chan *buffer, 1),
		state:     openState,
		pressureC: sync.NewCond(new(sync.Mutex)),
		relax:     make(chan struct{}),
	}
}

// ProxyOption are an encapsulation of configuration options
// appliable to the proxy.
type ProxyOption func(*Proxy)

// NewProxy create a new proxy configured with options options.
func NewProxy(options ...ProxyOption) *Proxy {

	p := &Proxy{
		cmds: make(chan func(*Proxy), 2),
	}

	for _, opt := range options {
		opt(p)
	}

	return p
}

// ListenAndServe starts the configured proxy p.
// This method blocks until p is stopped by
// calling shutdown.
func (p *Proxy) ListenAndServe() error {

	var err error

	if p.listenListener, err = StartServer(
		p.listenAddr, p.listenPort, p.handleClient); err != nil {
		return err
	}

	if p.eavesdropListener, err = StartServer(
		p.eavesdropAddr, p.eavesdropPort, p.handleEavesdrop); err != nil {
		p.listenListener.Close()
		p.listenListener = nil
		return err
	}

	for !p.done {
		select {
		case fn := <-p.cmds:
			fn(p)
		}
	}
	log.Debug("proxy l&s finished")

	return nil
}

// stopListening tears down the listening interfaces.
func (p *Proxy) stopListening() {
	for _, l := range []*net.Listener{&p.listenListener, &p.eavesdropListener} {
		if *l != nil {
			(*l).Close()
			*l = nil
		}
	}
}

// kill stops all sessions and stops the main loop
// of the proxy p. To be run in the main loop.
func (p *Proxy) kill(sem chan<- struct{}) {

	if p.done || p.noMoreSessions != nil {
		return
	}
	p.stopListening()

	if len(p.sessions) == 0 {
		p.done = true
		sem <- struct{}{}
		return
	}

	// We have to wait
	p.noMoreSessions = func(p *Proxy) {
		p.done = true
		p.noMoreSessions = nil
		sem <- struct{}{}
	}

	for _, s := range p.sessions {
		s.kill()
	}
}

// Shutdown terminates the proxy p.
func (p *Proxy) Shutdown() {
	sem := make(chan struct{})
	p.cmds <- func(p *Proxy) {
		log.Debug("proxy: shutdown")
		p.kill(sem)
	}
	<-sem
}

// dial establish a connection to the remote server.
func (p *Proxy) dial() (net.Conn, error) {
	return Dial(p.serverAddr, p.serverPort)
}

// attachClient attaches a new client to session s.
// If the session already has a client it bounces
// back to the managing proxy to create a new session
// to attach the client to.
func (s *session) attachClient(conn net.Conn) {

	log.Debug("session: attach client")

	// If we are going down or we are already have
	// a connected client spawn a new proxy session.
	if s.state.any(serverClosedReadState|serverClosedWriteState) ||
		s.server != nil {
		s.proxy.freshClient(conn)
		return
	}

	// Lazy dial remote server.
	var err error
	if s.server, err = s.proxy.dial(); err != nil {
		log.Debugf("Dialing server failed: %v", err)
		conn.Close()
		return
	}

	s.client = conn
	e := newClient(s, conn, true)
	s.clients = append(s.clients, e)
	go e.run()

	go s.clientToServer()
	go s.serverToClients(s.server)
}

// attachEavedropper attaches a new eavesdropper to session s.
// A session is allowed to have as many eavesdroppers as possible.
func (s *session) attachEavedropper(conn net.Conn) {
	if s.state.has(serverClosedReadState) {
		s.proxy.freshEavesdropper(conn)
		return
	}
	e := newClient(s, conn, false)
	s.clients = append(s.clients, e)
	go e.run()
}

func (p *Proxy) freshClient(conn net.Conn) {
	log.Debug("proxy: fresh client")
	p.cmds <- func(p *Proxy) {
		log.Debug("proxy: async freshClient")
		s := p.newSession()
		s.asyncAttachClient(conn)
	}
}

func (p *Proxy) freshEavesdropper(conn net.Conn) {
	log.Debug("proxy: fresh eavedrop")
	p.cmds <- func(p *Proxy) {
		s := p.newSession()
		log.Debug("proxy: async freshEavesdropper")
		s.asyncAttachEavedropper(conn)
	}
}

func (p *Proxy) newSession() *session {
	log.Debug("proxy: new session")
	s := newSession(p)
	p.sessions = append(p.sessions, s)
	go s.run()
	return s
}

// deactive removes a session s from the management of sessions
// of proxy p.
func (p *Proxy) deactive(s *session) {

	log.Debug("proxy: deactivate")

	// if its in the sessions we can finally shut it down.
	for i, t := range p.sessions {
		if t == s {
			copy(p.sessions[i:], p.sessions[i+1:])
			p.sessions[len(p.sessions)-1] = nil
			p.sessions = p.sessions[:len(p.sessions)-1]
			break
		}
	}

	log.Debugf("proxy: %d remaining sessions", len(p.sessions))

	// called if we are in a shutdown process
	if p.noMoreSessions != nil && len(p.sessions) == 0 {
		p.noMoreSessions(p)
	}
}

func (p *Proxy) attachClient(conn net.Conn) {
	log.Debug("proxy: attach client")
	if p.done || p.noMoreSessions != nil {
		conn.Close()
		return
	}
	var s *session
	if len(p.sessions) == 0 {
		s = p.newSession()
	} else {
		s = p.sessions[len(p.sessions)-1]
	}
	go s.asyncAttachClient(conn)
}

func (s *session) asyncAttachClient(conn net.Conn) {
	log.Debug("session: issue asyncAttachClient")
	s.cmds <- func(s *session) {
		log.Debug("session: asyncAttachClient")
		s.attachClient(conn)
	}
}

func (s *session) asyncAttachEavedropper(conn net.Conn) {
	log.Debug("session: issue asyncAttachEavedropper")
	s.cmds <- func(s *session) {
		log.Debug("session: asyncAttachEavedropper")
		s.attachEavedropper(conn)
	}
}

func (p *Proxy) attachEavedropper(conn net.Conn) {
	if p.done || p.noMoreSessions != nil {
		conn.Close()
		return
	}
	var s *session
	if len(p.sessions) == 0 {
		s = p.newSession()
	} else {
		s = p.sessions[len(p.sessions)-1]
	}
	// TODO: What if the session is already dead?
	go s.asyncAttachEavedropper(conn)
}

func (p *Proxy) handleClient(conn net.Conn) {
	if p.done || p.noMoreSessions != nil {
		conn.Close()
		return
	}
	p.asyncAttachClient(conn)
}

func (p *Proxy) asyncAttachClient(conn net.Conn) {
	log.Debug("proxy: issue asyncAttachClient")
	p.cmds <- func(p *Proxy) {
		log.Debug("proxy: asyncAttachClient")
		p.attachClient(conn)
	}
}

func (p *Proxy) handleEavesdrop(conn net.Conn) {
	if p.done || p.noMoreSessions != nil {
		conn.Close()
		return
	}

	log.Debug("handle eavesdrop")
	p.cmds <- func(p *Proxy) {
		log.Debug("proxy: async handleEavesdrop")
		p.attachEavedropper(conn)
	}
}

// clientToServer copies the the in-bound traffic from a bi-directional
// client to the server.
func (s *session) clientToServer() {

	defer func() {
		s.server.(closeReadWrite).CloseWrite()
		s.client.(closeReadWrite).CloseRead()
	}()

	log.Debug("copy client to server")

	if _, err := io.Copy(s.server, s.client); err != nil {
		log.Debugf("c2s copy err: %v", err)
	}

	s.asyncClose(serverClosedWriteState)
}

// read copies in-bound traffic from the bi-directional client
// to the remote server.
func (s *session) serverToClients(srv net.Conn) {

	defer srv.(closeReadWrite).CloseRead()

	var b *buffer
	for {
		s.pressureC.L.Lock()
		for s.pressure {
			s.pressureC.Wait()
		}
		s.pressureC.L.Unlock()

		if b == nil {
			b = allocBuffer()
		}

		n, err := srv.Read(b.buf)
		if n > 0 {
			b.buf = b.buf[:n]
			s.in <- b
			b = nil
		}
		if err != nil {
			s.in <- serverClosedSentinel
			s.asyncClose(serverClosedReadState)
			return
		}
	}
}

func (s *session) asyncClose(mask sessionState) {
	log.Debug("session: issue asyncClose")
	s.cmds <- func(s *session) {
		log.Debug("session: asyncClose")
		s.close(mask)
	}
}

// close updates the internal state of the session
// and signals the proxy to unregister it if its
// done.
func (s *session) close(mask sessionState) {

	if log.Level >= logrus.DebugLevel {
		log.Debugf(`session: close "%s" | "%s"`, mask, s.state)
	}

	if s.state |= mask; s.state.has(closedState) {

		log.Debug("session: shutdown")

		if len(s.clients) == 0 {
			s.proxy.asyncDeactivate(s)
		} else {
			s.state |= waitRemainingClientsState
		}
	}
}

func (p *Proxy) asyncDeactivate(s *session) {
	log.Debug("proxy: issue asyncDeactivate")
	p.cmds <- func(p *Proxy) {
		log.Debug("proxy: syncDeactivate")
		p.deactive(s)
	}
}

// remove removes an eavesdropper e from the session s.
func (s *session) remove(e *client) {
	log.Debugf("session: remove client (%d connected)", len(s.clients))

	for i, x := range s.clients {
		if x == e {
			copy(s.clients[i:], s.clients[i+1:])
			s.clients[len(s.clients)-1] = nil
			s.clients = s.clients[:len(s.clients)-1]
			break
		}
	}

	if e.bidirectional {
		s.close(clientClosedState)
	}

	if s.state.has(waitRemainingClientsState) && len(s.clients) == 0 {
		s.state &^= waitRemainingClientsState
		s.proxy.asyncDeactivate(s)
	}
}

// kill disconnects all clients and stops the main loop
// of the session s.
func (s *session) kill() {
	log.Debug("session: kill")

	if s.server != nil {
		s.server.Close()
	}
	if s.client != nil {
		s.client.Close()
	}

	// In case we are waiting for full buffers
	// to relax.
	s.pressureC.L.Lock()
	s.pressure = false
	s.pressureC.L.Unlock()
	s.pressureC.Signal()

	// In case there is no bi-directional client
	// fake its shutdown to quit the session.
	if s.client == nil {
		s.asyncClose(clientClosedState | serverClosedWriteState)
	}

	// If only clients are connected there is
	// no server to send the terminating sentinel buffer.
	// So fake it.
	if s.server == nil {
		s.cmds <- func(s *session) {
			s.close(serverClosedReadState)
			s.broadcast(serverClosedSentinel)
		}
	}
}

// run is the main delivery loop of packet received from
// the server.
func (e *client) run() {
	log.Debug("eavesdrop run")

	var copied int64

	defer func() {
		log.Debugf("client copied %d bytes", copied)
		if e.bidirectional {
			e.conn.(closeReadWrite).CloseWrite()
		} else {
			e.conn.Close()
		}
	}()

	timeout := e.session.proxy.timeout

	for {
		var b *buffer

		e.buffersC.L.Lock()

		for e.buffers.empty() {
			e.buffersC.Wait()
		}

		bufLen := e.buffers.len()

		b = e.buffers.popFront()

		e.buffersC.L.Unlock()

		if b == serverClosedSentinel {
			e.session.asyncRemove(e)
			return
		}

		// We will relax if we have exactly
		// bufferFull buffers before removing.
		if bufLen == bufferFull {
			e.session.relax <- struct{}{}
		}

		for buf := b.buf; len(buf) > 0; {
			if timeout > 0 {
				e.conn.SetWriteDeadline(time.Now().Add(timeout))
			}
			n, err := e.conn.Write(buf)
			if err != nil {
				if nerr, ok := err.(*net.OpError); ok && nerr.Timeout() {
					log.Debugf("client timed out: %v", err)
				}
				b.dec()
				e.dropBuffers()
				e.session.relax <- struct{}{}
				e.session.asyncRemove(e)
				return
			}
			buf = buf[n:]
			copied += int64(n)
		}
		b.dec()
	}
}

func (s *session) asyncRemove(c *client) {
	//lg.Debug("session: issue asyncRemove")
	s.cmds <- func(s *session) {
		//lg.Debug("session: asyncRemove")
		s.remove(c)
	}
}

// dropBuffers releases the remaining buffers of an eavedropper e.
func (e *client) dropBuffers() {
	e.buffersC.L.Lock()
	for !e.buffers.empty() {
		if b := e.buffers.popFront(); b != serverClosedSentinel {
			b.dec()
		}
	}
	e.buffers = nil
	e.buffersC.L.Unlock()
}

// run drives the main go routine of a session.
// All modifications to the session data structure should
// be placed in the session's cmd channel.
func (s *session) run() {
	log.Debug("session: run")
	for s.state != closedState {
		select {
		case fn := <-s.cmds:
			fn(s)
		case in := <-s.in:
			p := s.broadcast(in)
			s.pressureC.L.Lock()
			s.pressure = p
			s.pressureC.L.Unlock()
			// No need to wake up when still under pressure.
			if !p {
				s.pressureC.Signal()
			}
		case <-s.relax:
			s.pressureC.L.Lock()
			p := s.pressure
			s.pressureC.L.Unlock()
			// Only wake up if there is an improvement.
			if p && !s.bufferFull() {
				s.pressureC.L.Lock()
				s.pressure = false
				s.pressureC.L.Unlock()
				s.pressureC.Signal()
			}
		}
	}
	log.Debug("session run: finished")
}

// broadcast enqueues a data buffer b received
// from the remote server a the out queues
// of the connected clients.
func (s *session) broadcast(b *buffer) bool {

	if b != serverClosedSentinel {
		n := len(s.clients)
		if n == 0 {
			b.free()
			return false
		}
		b.inc(n)
	}

	var full bool
	for _, e := range s.clients {
		if e.consume(b) {
			full = true
		}
	}
	return full
}

// bufferFull indicates if there is pressure on a
// buffer queue of a client.
const bufferFull = 10

// bufferFull tests if there is a client with a full buffer.
func (s *session) bufferFull() bool {
	for _, c := range s.clients {
		if c.bufferFull() {
			return true
		}
	}
	return false
}

// bufferFull indicates that a client has a full buffer.
func (e *client) bufferFull() bool {
	e.buffersC.L.Lock()
	full := e.buffers != nil && e.buffers.len() >= bufferFull
	e.buffersC.L.Unlock()
	return full
}

// consume enqueues a buffer b at the end
// of the out-bound data queue of a client.
// The potentially waiting delivery go routine
// is signaled.
func (e *client) consume(b *buffer) bool {
	var full bool
	e.buffersC.L.Lock()
	if e.buffers != nil {
		e.buffers.pushBack(b)
		full = e.buffers.len() >= bufferFull
	} else if b != serverClosedSentinel {
		// client cannot consume it any more.
		b.dec()
	}
	e.buffersC.L.Unlock()
	e.buffersC.Signal()
	return full
}

// ProxyServerAddr configures the address of the remote host to connect to.
func ProxyServerAddr(addr string) ProxyOption {
	return func(p *Proxy) { p.serverAddr = addr }
}

// ProxyServerPort configures the port of the remote host to connect to.
func ProxyServerPort(port int) ProxyOption {
	return func(p *Proxy) { p.serverPort = port }
}

// ProxyEavesdropAddr configures the address the eavesdroppers can connect to.
func ProxyEavesdropAddr(addr string) ProxyOption {
	return func(p *Proxy) { p.eavesdropAddr = addr }
}

// ProxyEavesdropPort configures the port of eavesdroppers can connect to.
func ProxyEavesdropPort(port int) ProxyOption {
	return func(p *Proxy) { p.eavesdropPort = port }
}

// ProxyListenAddr configures the address the bidirectional clients can connect to.
func ProxyListenAddr(addr string) ProxyOption {
	return func(p *Proxy) { p.listenAddr = addr }
}

// ProxyListenPort configures the port the bidirectional clients can connect to.
func ProxyListenPort(port int) ProxyOption {
	return func(p *Proxy) { p.listenPort = port }
}

// ProxyTimeout configures the duration to wait until write to a client fails.
func ProxyTimeout(d time.Duration) ProxyOption {
	return func(p *Proxy) { p.timeout = d }
}
