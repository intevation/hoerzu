// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.
package hoerzu

import (
	"io"
	"io/ioutil"
	"net"
	"testing"
	"time"
)

func TestConfig(t *testing.T) {

	// TODO: Test all config values.

	p := NewProxy(ProxyServerAddr("localhost"), ProxyServerPort(666))

	if p.serverAddr != "localhost" {
		t.Fatalf("want 'localhost' got '%s'", p.serverAddr)
	}

	if p.serverPort != 666 {
		t.Fatalf("want 666 got '%d'", p.serverPort)
	}
}

//func TestServerProxyClient(t *testing) {
func TestStartServer(t *testing.T) {
	handler := func(c net.Conn) { c.Close() }
	ls, err := StartServer("127.0.0.1", 0, handler)
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	ls.Close()
}

func TestServerClient(t *testing.T) {

	hCh := make(chan []byte)

	handler := func(c net.Conn) {
		go func() {
			defer c.Close()
			data, err := ioutil.ReadAll(c)
			if err != nil {
				t.Fatalf("error while reading: %v\n", err)
			}
			hCh <- data
		}()
	}

	ls, err := StartServer("127.0.0.1", 0, handler)
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	cl, err := Dial("127.0.0.1", sPort)
	if err != nil {
		t.Fatalf("cannot connect to proxy: %v\n", err)
	}

	const msg = "Hello, World!"

	go func() {
		if _, err = cl.Write([]byte(msg)); err != nil {
			t.Fatalf("write to server failed: %v\n", err)
		}
		cl.Close()
	}()

	if data := string(<-hCh); data != msg {
		t.Errorf("Got %s expected %s\n", data, msg)
	}
}

func TestServerProxyClient(t *testing.T) {

	hCh := make(chan []byte)

	handler := func(c net.Conn) {
		go func() {
			defer c.Close()
			data, err := ioutil.ReadAll(c)
			if err != nil {
				t.Fatalf("error while reading: %v\n", err)
			}
			hCh <- data
		}()
	}

	ls, err := StartServer("127.0.0.1", 0, handler)
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}

	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	lPortCh := make(chan int)

	go func() {
		go func() {
			p.cmds <- func(p *Proxy) {
				lPort := p.listenListener.Addr().(*net.TCPAddr).Port
				lPortCh <- lPort
			}
		}()
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	lPort := <-lPortCh

	cl, err := Dial("127.0.0.1", lPort)
	if err != nil {
		t.Fatalf("cannot connect to proxy: %v\n", err)
	}

	const msg = "Hello, World!"

	if _, err = cl.Write([]byte(msg)); err != nil {
		t.Fatalf("write to proxy failed: %v\n", err)
	}
	cl.Close()

	data := string(<-hCh)
	if data != msg {
		t.Errorf("Got %s expected %s\n", data, msg)
	}

	p.Shutdown()
}

func echoHandler(t *testing.T) func(c net.Conn) {
	return func(c net.Conn) {
		go func() {
			defer c.Close()
			if _, err := io.Copy(c, c); err != nil {
				t.Logf("server error: %v\n", err)
			}
		}()
	}
}

func TestOnlyEavedropper(t *testing.T) {
	LogLevel("debug")

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan int)

	go func() {
		p.cmds <- func(p *Proxy) {
			port := p.eavesdropListener.Addr().(*net.TCPAddr).Port
			portCh <- port
		}
	}()

	go func() {
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	port := <-portCh

	cl, err := Dial("127.0.0.1", port)
	if err != nil {
		t.Fatalf("cannot connect to proxy: %v\n", err)
	}
	defer cl.Close()

	time.Sleep(time.Second / 4)

	// no traffic. hard shutdown.

	p.Shutdown()
}

func TestManySessions(t *testing.T) {
	LogLevel("debug")

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan int)

	go func() {
		p.cmds <- func(p *Proxy) {
			port := p.listenListener.Addr().(*net.TCPAddr).Port
			portCh <- port
		}
	}()

	go func() {
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	port := <-portCh

	for i := 0; i < 20; i++ {
		//time.Sleep(time.Second / 10)
		log.Debugf("connecting client %d", i)
		cl, err := Dial("127.0.0.1", port)
		if err != nil {
			t.Fatalf("cannot connect to proxy: %v\n", err)
		}
		defer cl.Close()
	}

	time.Sleep(time.Second / 2)

	// no traffic. hard shutdown.
	p.Shutdown()
}

func TestAfterShutdown(t *testing.T) {
	LogLevel("debug")

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan [2]int)

	go func() {
		p.cmds <- func(p *Proxy) {
			clPort := p.listenListener.Addr().(*net.TCPAddr).Port
			ePort := p.eavesdropListener.Addr().(*net.TCPAddr).Port
			portCh <- [2]int{clPort, ePort}
		}
	}()

	go func() {
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	ports := <-portCh

	p.Shutdown()

	for _, p := range ports {
		cl, err := Dial("127.0.0.1", p)
		if cl != nil {
			defer cl.Close()
		}
		if err == nil {
			t.Error("connecting client after shutdown succeed")
		}
	}

}

func TestBadRemoteAddress(t *testing.T) {

	LogLevel("debug")

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	p := NewProxy(
		ProxyServerAddr("0.0.0.0"), ProxyServerPort(0),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan int)

	go func() {
		p.cmds <- func(p *Proxy) {
			port := p.listenListener.Addr().(*net.TCPAddr).Port
			portCh <- port
		}
	}()

	go func() {
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	port := <-portCh
	cl, _ := Dial("127.0.0.1", port)
	if cl != nil {
		defer cl.Close()
	}

	// should unblock with resetting connection by server
	cl.Read(make([]byte, 1))

	p.Shutdown()
}

func TestStartTwice(t *testing.T) {

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	p1 := NewProxy(
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan [2]int)
	go func() {
		p1.cmds <- func(p *Proxy) {
			clPort := p.listenListener.Addr().(*net.TCPAddr).Port
			ePort := p.eavesdropListener.Addr().(*net.TCPAddr).Port
			portCh <- [2]int{clPort, ePort}
		}
	}()

	go func() {
		if err := p1.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	defer p1.Shutdown()

	ports := <-portCh

	p2 := NewProxy(
		ProxyServerAddr("0.0.0.0"), ProxyServerPort(0),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(ports[0]),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	if err := p2.ListenAndServe(); err == nil {
		t.Errorf("started listen server on same port twice")
		p2.Shutdown()
	}

	p3 := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(0),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(ports[1]))

	if err := p3.ListenAndServe(); err == nil {
		t.Errorf("started eavesdrop server on same port twice")
		p3.Shutdown()
	}
}

func TestTimeout(t *testing.T) {
	LogLevel("debug")

	ls, err := StartServer("127.0.0.1", 0, echoHandler(t))
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}
	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyTimeout(time.Second/2),
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan int)

	go func() {
		p.cmds <- func(p *Proxy) {
			port := p.listenListener.Addr().(*net.TCPAddr).Port
			portCh <- port
		}
	}()

	go func() {
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	port := <-portCh

	cl, err := Dial("127.0.0.1", port)
	if err != nil {
		t.Fatalf("cannot connect to proxy: %v\n", err)
	}
	defer cl.Close()

	sem := make(chan struct{})

	go func() {
		// closing read is crucial to prevent a proxy waiting
		// input forever.
		cl.(*net.TCPConn).CloseRead()
		if _, err := io.Copy(cl, generate(60*1024*1024)); err != nil {
			log.Debugf("error: %v", err)
		}
		sem <- struct{}{}
	}()

	<-sem

	p.Shutdown()
}
