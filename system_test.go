// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.
package hoerzu

import (
	"bytes"
	"crypto/sha256"
	"io"
	"math/rand"
	"net"
	"sync"
	"testing"
	"time"
)

func TestSystem(t *testing.T) {
	startSystem(3*1024*1024, 20, t)
}

func generate(size int) io.Reader {
	r := rand.New(rand.NewSource(1))
	return io.LimitReader(r, int64(size))
}

type slowReader struct {
	io.Reader
}

func (sr *slowReader) Read(b []byte) (int, error) {
	n, err := sr.Reader.Read(b)
	time.Sleep(20 * time.Millisecond)
	return n, err
}

func startSystem(size, numEavesdropper int, t *testing.T) {

	type result struct {
		num   int
		sum   []byte
		bytes int64
	}

	srvCh := make(chan result)

	handler := func(c net.Conn) {
		go func() {
			sum := sha256.New()
			tin := io.TeeReader(c, sum)
			var b int64
			b, err := io.Copy(c, tin)
			if err != nil {
				t.Fatalf("server error: %v\n", err)
			}
			c.Close()
			srvCh <- result{sum: sum.Sum(nil), bytes: b}
		}()
	}

	ls, err := StartServer("127.0.0.1", 0, handler)
	if err != nil {
		t.Fatalf("cannot start remote server: %v\n", err)
	}

	defer ls.Close()

	sPort := ls.Addr().(*net.TCPAddr).Port

	p := NewProxy(
		ProxyServerAddr("127.0.0.1"), ProxyServerPort(sPort),
		ProxyListenAddr("127.0.0.1"), ProxyListenPort(0),
		ProxyEavesdropAddr("127.0.0.1"), ProxyEavesdropPort(0))

	portCh := make(chan [2]int)

	go func() {
		go func() {
			p.cmds <- func(p *Proxy) {
				lPort := p.listenListener.Addr().(*net.TCPAddr).Port
				ePort := p.eavesdropListener.Addr().(*net.TCPAddr).Port

				portCh <- [2]int{lPort, ePort}
			}
		}()
		if err := p.ListenAndServe(); err != nil {
			t.Fatalf("cannot start proxy server: %v\n", err)
		}
	}()

	ports := <-portCh

	cl, err := Dial("127.0.0.1", ports[0])
	if err != nil {
		t.Fatalf("cannot connect to proxy: %v\n", err)
	}

	// To ensure that all clients consume the same amount
	// of data do some extra effort to synchronize their
	// start of reading.
	// If this is not done some clients will already have
	// read data before the last ones kick in having missed the
	// leading data.
	var clientsReady sync.WaitGroup
	clientsReady.Add(1 + numEavesdropper)

	send := make(chan result)
	go func() {
		in := generate(size)
		sum := sha256.New()
		tin := io.TeeReader(in, sum)

		// wait until all clients are ready to receive.
		clientsReady.Wait()
		// some little extra time to block the clients
		// in their io.Copy loop.
		time.Sleep(time.Second / 50)
		b, err := io.Copy(cl, tin)
		if err != nil {
			t.Fatalf("writing data failed: %v\n", err)
		}
		cl.(closeReadWrite).CloseWrite()
		send <- result{sum: sum.Sum(nil), bytes: b}
	}()

	clCh := make(chan result)
	go func() {
		sum := sha256.New()
		var b int64

		sr := &slowReader{cl}
		clientsReady.Done()
		b, err := io.Copy(sum, sr)
		if err != nil {
			t.Fatalf("writing data failed: %v\n", err)
		}
		cl.(closeReadWrite).CloseRead()
		clCh <- result{sum: sum.Sum(nil), bytes: b}
	}()

	var wg sync.WaitGroup

	var resMu sync.Mutex
	var res []result

	for i := 0; i < numEavesdropper; i++ {
		evCl, err := Dial("127.0.0.1", ports[1])
		if err != nil {
			t.Fatalf("cannot connect to proxy: %v\n", err)
		}
		wg.Add(1)
		go func(num int) {
			defer evCl.Close()
			defer wg.Done()
			sum := sha256.New()

			clientsReady.Done()

			b, err := io.Copy(sum, evCl)
			if err != nil {
				t.Fatalf("eavesdropper copy failed: %v\n", err)
			}
			resMu.Lock()
			res = append(res, result{num, sum.Sum(nil), b})
			resMu.Unlock()
		}(i)
	}

	log.Debug("before want")

	want := <-send

	log.Debug("after want")

	srvGot := <-srvCh

	log.Debug("after server")

	clGot := <-clCh

	log.Debug("after client")

	if bytes.Compare(want.sum, clGot.sum) != 0 {
		t.Errorf("client got wrong data\n")
	}

	if want.bytes != clGot.bytes {
		t.Errorf("bytes send: %d, client got: %d\n",
			want.bytes, clGot.bytes)
	}

	if bytes.Compare(want.sum, srvGot.sum) != 0 {
		t.Errorf("server got wrong data\n")
	}

	if want.bytes != srvGot.bytes {
		t.Errorf("bytes send: %d, server got: %d\n",
			want.bytes, srvGot.bytes)
	}

	wg.Wait()

	for _, have := range res {
		if bytes.Compare(want.sum, have.sum) != 0 {
			t.Errorf("eavesdropper %d failed\n", have.num)
		}
		if want.bytes != have.bytes {
			t.Errorf("eavesdropper %d failed. bytes send: %d, got %d\n",
				have.num, want.bytes, have.bytes)
		}
	}

	// TODO: stop system

	p.Shutdown()
}
