package hoerzu

// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.

import (
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

var (
	// log is the package logger.
	logMu sync.Mutex
	log   = logrus.New()
)

// LogLevel set the package log level.
func LogLevel(lvl string) {
	l, err := logrus.ParseLevel(lvl)
	if err != nil {
		l = logrus.WarnLevel
	}
	logMu.Lock()
	log.SetLevel(l)
	logMu.Unlock()
}

// LogFile directs logging from this package to the given file.
// An empty string redirects the logging to os.Stderr.
func LogFile(file string) {
	if file != "" {
		f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err == nil {
			logMu.Lock()
			log.Out = f
			logMu.Unlock()
		} else {
			log.Warn("open log-file failed")
		}
	} else {
		logMu.Lock()
		log.Out = os.Stderr
		logMu.Unlock()
	}
}
