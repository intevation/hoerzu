// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.
package hoerzu

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestBadLogLevel(t *testing.T) {
	LogLevel("")
}

func TestBadFile(t *testing.T) {
	defer LogFile("")
	LogFile("/")
}

func TestLogFile(t *testing.T) {
	defer LogFile("")
	tmpfile, err := ioutil.TempFile("", "logfile")
	if err != nil {
		t.Fatal(err)
	}
	name := tmpfile.Name()
	defer os.Remove(name)
	LogFile(name)
	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}
}
