# Performance

## relay throughput - no eavesdroppers

### Setup
echo server build with [`socat`](http://www.dest-unreach.org/socat/doc/socat.html).  
Piping in `/dev/zero` via [`netcat`](http://nc110.sourceforge.net/)
measuring the bounced data rate with [`pv`](http://www.ivarch.com/programs/pv.shtml)
sunk into `/dev/null`.   
For comparison the rates over a `socat` and a `hoerzu` relay.  
Measured on four different GNU/Linux systems until the average rate `pv -a` stabilized.

| system    | direct [MiB/s] | socat relay [MiB/s] | hoerzu [MiB/s] |
| ---------:| --------------:| -------------------:| --------------:|
|     rasp3 |     137 (100%) |            99 (72%) |       86 (62%) |
|        i5 |     656 (100%) |           571 (87%) |      526 (80%) |
|     i7old |     341 (100%) |           329 (96%) |      254 (74%) |
|     i7new |     784 (100%) |           635 (80%) |      613 (78%) |
|      xeon |     640 (100%) |           612 (94%) |      634 (97%) |

### echo server

```
socat TCP-LISTEN:8080,fork exec:'/bin/cat'
```

### direct

```
netcat localhost 8080 < /dev/zero | pv -a > /dev/null
```

### measuring over relay

```
netcat localhost 8899 < /dev/zero | pv -a > /dev/null
```

### `socat` relay

```
socat TCP-LISTEN:8899,fork TCP-CONNECT:localhost:8080
```

### `hoerzu` relay

```
hoerzu --listen 127.0.0.1
```

### systems

1. **rasp3** Raspbian GNU/Linux 9 / 4.9.41-v7+ / ARMv7 Processor rev 4 (v7l) (Raspberry Pi 3)
2. **i5** Ubuntu 17.10 / 4.13.0-12-generic / Intel(R) Core(TM) i5-5250U CPU @ 1.60GHz
3. **i7old** Arch Linux / 4.13.3-1-ARCH / Intel(R) Core(TM) i7 CPU 860 @ 2.80GHz
4. **i7new** Ubuntu 17.10 / 4.13.0-12-generic / Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
5. **xeon** Debian GNU/Linux 8 / 4.9.0-0.bpo.1-amd64 / Intel(R) Xeon(R) CPU E5-2620 v4 @ 2.10GHz

## TODOs

* Measure with eavesdroppers
* Measure latency

