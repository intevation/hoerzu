#!/bin/sh
go get -u -v github.com/pkg/errors
go get -u -v gopkg.in/urfave/cli.v1
go get -u -v github.com/urfave/cli/altsrc
go get -u -v github.com/sirupsen/logrus
