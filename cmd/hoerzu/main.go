// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.
package main

import (
	"os"
	"os/signal"

	"bitbucket.org/intevation/hoerzu"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/altsrc"

	cli "gopkg.in/urfave/cli.v1"
)

func main() {
	app := cli.NewApp()

	app.Name = appName
	app.Version = appVersion
	app.Usage = appUsage

	app.Flags = []cli.Flag{
		altsrc.NewStringFlag(cli.StringFlag{
			Name:  "remote",
			Value: "localhost",
			Usage: "remote `HOST` to connect to",
		}),
		altsrc.NewIntFlag(cli.IntFlag{
			Name:  "remote-port",
			Value: 8080,
			Usage: "remote `PORT` to connect to",
		}),
		altsrc.NewStringFlag(cli.StringFlag{
			Name:  "listen",
			Value: "localhost",
			Usage: "listen `HOST`",
		}),
		altsrc.NewIntFlag(cli.IntFlag{
			Name:  "listen-port",
			Value: 8899,
			Usage: "listen `PORT`",
		}),
		altsrc.NewStringFlag(cli.StringFlag{
			Name:  "eavesdrop",
			Value: "localhost",
			Usage: "eavesdrop `HOST`",
		}),
		altsrc.NewIntFlag(cli.IntFlag{
			Name:  "eavesdrop-port",
			Value: 8889,
			Usage: "eavesdrop `PORT`",
		}),
		altsrc.NewDurationFlag(cli.DurationFlag{
			Name:  "timeout",
			Value: 0,
			Usage: "`DURATION` before disconnecting a stale client. 0s: no timeout",
		}),
		altsrc.NewStringFlag(cli.StringFlag{
			Name:  "log-level",
			Value: "info",
			Usage: "Log `LEVEL`: debug, info, warn, error, fatal, panic",
		}),
		altsrc.NewStringFlag(cli.StringFlag{
			Name:  "log-file",
			Value: "",
			Usage: "write log in `FILE`",
		}),
		cli.StringFlag{
			Name:  "load",
			Usage: "load configuration from `FILE`",
		},
	}
	app.Before = loadConfig
	app.Action = runProxy

	app.Run(os.Args)
}

func runProxy(c *cli.Context) error {

	hoerzu.LogLevel(c.String("log-level"))
	hoerzu.LogFile(c.String("log-file"))

	p := hoerzu.NewProxy(
		hoerzu.ProxyServerAddr(c.String("remote")),
		hoerzu.ProxyServerPort(c.Int("remote-port")),
		hoerzu.ProxyListenAddr(c.String("listen")),
		hoerzu.ProxyListenPort(c.Int("listen-port")),
		hoerzu.ProxyEavesdropAddr(c.String("eavesdrop")),
		hoerzu.ProxyEavesdropPort(c.Int("eavesdrop-port")),
		hoerzu.ProxyTimeout(c.Duration("timeout")))

	errCh := make(chan error)

	go func() { errCh <- p.ListenAndServe() }()

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt, os.Kill)

	var err error
	var s os.Signal
	select {
	case s = <-sigCh:
	case err = <-errCh:
	}

	if s != nil {
		p.Shutdown()
		err = <-errCh
	}

	if err != nil {
		logrus.Errorf("error: %v", err)
	}
	return err
}

// loadConfig loads a configuration from a file.
func loadConfig(c *cli.Context) error {
	// If the optional config file is missing the program
	// would silently die w/o simply taking the defaults,
	// so we have to check this ourself before.
	file := c.String("load")
	if file == "" {
		return nil
	}
	if _, err := os.Stat(file); os.IsNotExist(err) {
		logrus.Warn("config file %s does not exist", file)
		return nil
	}
	fn := altsrc.InitInputSourceWithContext(
		c.App.Flags, altsrc.NewTomlSourceFromFlagFunc("load"))
	return fn(c)
}
