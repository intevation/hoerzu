// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.
package main

const (
	appName    = "hoerzu"
	appVersion = "0.1"
	appUsage   = "A simple eavesdropping TCP proxy server."
)
