package hoerzu

// Copyright (c) 2013-2017, Peter H. Froehlich. All rights reserved.
// Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

// Package queue implements a double-ended queue (aka "deque") data structure
// on top of a slice. All operations run in (amortized) constant time.
// Benchmarks compare favorably to container/list as well as to Go's channels.
// These queues are not safe for concurrent use.

// bufferQueue represents a double-ended queue.
// The zero value is an empty queue ready to use.
type bufferQueue struct {
	// pushBack writes to rep[back] then increments back; pushFront
	// decrements front then writes to rep[front]; len(rep) is a power
	// of two; unused slots are nil and not garbage.
	rep      []*buffer
	frontPos int
	backPos  int
	length   int
}

// New returns an initialized empty queue.
func newBufferQueue() *bufferQueue {
	return new(bufferQueue).init()
}

// Init initializes or clears queue q.
func (q *bufferQueue) init() *bufferQueue {
	q.rep = make([]*buffer, 1)
	q.frontPos, q.backPos, q.length = 0, 0, 0
	return q
}

// Len returns the number of elements of queue q.
func (q *bufferQueue) len() int {
	return q.length
}

// empty returns true if the queue q has no elements.
func (q *bufferQueue) empty() bool {
	return q.length == 0
}

// full returns true if the queue q is at capacity.
func (q *bufferQueue) full() bool {
	return q.length == len(q.rep)
}

// sparse returns true if the queue q has excess capacity.
func (q *bufferQueue) sparse() bool {
	return 1 < q.length && q.length < len(q.rep)/4
}

// resize adjusts the size of queue q's underlying slice.
func (q *bufferQueue) resize(size int) {
	adjusted := make([]*buffer, size)
	if q.frontPos < q.backPos {
		// rep not "wrapped" around, one copy suffices
		copy(adjusted, q.rep[q.frontPos:q.backPos])
	} else {
		// rep is "wrapped" around, need two copies
		n := copy(adjusted, q.rep[q.frontPos:])
		copy(adjusted[n:], q.rep[:q.backPos])
	}
	q.rep = adjusted
	q.frontPos = 0
	q.backPos = q.length
}

// lazyGrow grows the underlying slice if necessary.
func (q *bufferQueue) lazyGrow() {
	if q.full() {
		q.resize(len(q.rep) * 2)
	}
}

// lazyShrink shrinks the underlying slice if advisable.
func (q *bufferQueue) lazyShrink() {
	if q.sparse() {
		q.resize(len(q.rep) / 2)
	}
}

// inc returns the next integer position wrapping around queue q.
func (q *bufferQueue) inc(i int) int {
	return (i + 1) & (len(q.rep) - 1) // requires l = 2^n
}

// pushBack inserts a new value v at the back of queue q.
func (q *bufferQueue) pushBack(v *buffer) {
	q.lazyGrow()
	q.rep[q.backPos] = v
	q.backPos = q.inc(q.backPos)
	q.length++
}

// popFront removes and returns the first element of queue q or nil.
func (q *bufferQueue) popFront() *buffer {
	var v *buffer
	if !q.empty() {
		v = q.rep[q.frontPos]
		q.rep[q.frontPos] = nil // unused slots must be nil
		q.frontPos = q.inc(q.frontPos)
		q.length--
		q.lazyShrink()
	}
	return v
}
