# hoerzu - a small eavesdropping TCP proxy

**hoerzu** is a TCP relay/proxy between a remote server and clients.
There are two kinds of clients:

1. **Bidirectional**: Clients doing two-way communication with the remote server over the **hoerzu** proxy.
2. **Eavesdropping**: Clients listening to the data comming from the remote server. Traffic from the bidirectional clients
   is not recorded.

Each connected bidirectional client starts a *session*. A session has exactly one bidirectional client.
Eavesdropping clients can connect or disconnect to or from a session to follow the traffic send from the
remote server to the bidirectional client. A session is killed when the connection to the bidirectional is cut
or the remote server cuts the connection to the **hoerzu** server.

## Build

You need a working [Go](https://golang.org) build environment (Tested successfully with Go 1.9+).

```
go get -u -v bitbucket.org/intevation/hoerzu/cmd/hoerzu
```

Place the resulting `hoerzu` binary into your PATH.

## Usage

To see all options use the `-h`/`--help` flag:

```
NAME:
   hoerzu - A simple eavesdropping TCP proxy server.

USAGE:
   hoerzu [global options] command [command options] [arguments...]

VERSION:
   0.1

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --remote HOST          remote HOST to connect to (default: "localhost")
   --remote-port PORT     remote PORT to connect to (default: 8080)
   --listen HOST          listen HOST (default: "localhost")
   --listen-port PORT     listen PORT (default: 8899)
   --eavesdrop HOST       eavesdrop HOST (default: "localhost")
   --eavesdrop-port PORT  eavesdrop PORT (default: 8889)
   --timeout DURATION     DURATION before disconnecting a stale client. 0s: no timeout (default: 0s)
   --log-level LEVEL      Log LEVEL: debug, info, warn, error, fatal, panic (default: "info")
   --log-file FILE        write log in FILE
   --load FILE            load configuration from FILE
   --help, -h             show help
   --version, -v          print the version
```


The options can be stored in a TOML file and used with the `--load` flag.  
See [example.toml](example.toml) for an example.

## Performance
Some preliminary [measurements](performance.md).

## License

(c) 2017 Intevation GmbH. Development sponsored by Siemens AG.  
This is Free Software covered by the terms of the Apache 2 license.  
See [LICENSE](LICENSE) for details.
