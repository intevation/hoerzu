package hoerzu

// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.

import (
	"net"
	"strconv"

	"github.com/pkg/errors"
)

// StartServer starts an TCP server on a given address addr and port port.
// Accepted connections are forward to a given handler handler.
func StartServer(addr string, port int, handler func(net.Conn)) (net.Listener, error) {

	names, err := net.LookupHost(addr)
	if err != nil {
		return nil, errors.Wrap(err, "No address found")
	}
	if len(names) == 0 {
		return nil, errors.Errorf("No address found for '%s'.", addr)
	}

	addr = net.JoinHostPort(names[0], strconv.Itoa(port))

	ls, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, errors.Wrap(err, "cannot open listen address.")
	}

	log.Debugf("listen on: %v", addr)

	go func() {
		for {
			conn, err := ls.Accept()
			if err != nil {
				return
			}
			handler(conn)
		}
	}()

	return ls, nil
}

// Dial establish a connection to a TCP server
// given by address addr and port port.
func Dial(addr string, port int) (net.Conn, error) {
	names, err := net.LookupHost(addr)
	if err != nil {
		return nil, errors.Wrapf(err, "Cannot resolve '%s'", addr)
	}
	if len(names) == 0 {
		return nil, errors.Wrapf(err, "No address found for '%s'.", addr)
	}

	var server net.Conn

	portS := strconv.Itoa(port)
	for _, name := range names {
		addr := net.JoinHostPort(name, portS)
		server, err = net.Dial("tcp", addr)
		if err == nil {
			break
		}
	}

	if err != nil {
		err = errors.Wrap(err, "Dialing failed")
	}

	return server, err
}
