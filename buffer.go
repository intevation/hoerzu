package hoerzu

// This is Free Software covered by the terms of the Apache 2.0 license.
// See LICENSE file for details.
// (c) 2017 by Intevation GmbH.

import (
	"sync"
	"sync/atomic"
)

// buffer is a chunk of data bytes received from the server.
// To not copy the data many times (for each connected client)
// buffers are reference counted and managed with a global
// pool to take some preassure from memory alloction.
type buffer struct {
	// reference count how often this buffer is distrubted.
	count int32
	// the data of this chunk.
	buf []byte
}

// bufSize is the size of an allocated memory chunk.
// 20K is a little compromise bewtween
// high throughput (larger chunks) and latency (smaller chunks).
const bufSize = 20 * 1024

// bufferPool is the global pool for memory re-use.
var bufferPool = sync.Pool{
	New: func() interface{} {
		return &buffer{
			buf: make([]byte, bufSize),
		}
	},
}

// allocBuffer returns a buffer blown up to its full capacity.
func allocBuffer() *buffer {
	b := bufferPool.Get().(*buffer)
	b.count = 0
	b.buf = b.buf[:cap(b.buf)]
	return b
}

// inc increases the internal reference counter of buffer b.
func (b *buffer) inc(delta int) {
	atomic.AddInt32(&b.count, int32(delta))
}

// dec decreases the reference counter of b by one and
// recycles it if it went to zero. Using b after recycling
// is not permitted.
func (b *buffer) dec() {
	if atomic.AddInt32(&b.count, -1) == 0 {
		bufferPool.Put(b)
	}
}

func (b *buffer) free() {
	bufferPool.Put(b)
}
